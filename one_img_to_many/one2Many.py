# Performs Mosaic Augmentation on given image
# Ratio in decrements, think of this as the size of images
# Strides is the number of strides taken per x y axis, more strides, more images.
# write = True means we wish to write to file. If false, we simply view the images
"""Opening and closing Images actually takes a lot of RAM, for large files, my god it crashed my pc Hence The slow displaying of images"""
ratio = [95,20]
n_strides = 5 # Initial Stride
write = True
ubuntu = False
folder_pth = 'datasets/dusun_durian/'
original_file_name = 'image'

img_pth = folder_pth + original_file_name +".png"
labelme_json_pth = folder_pth + original_file_name+ ".json"
multipler = 1.65 # Increase n_strides as we decrease the size of images


import os
from util import *
from util_rect import *
import time
import matplotlib.pyplot as plt
import json
import PIL.Image
from PIL import ImageDraw

f = open(labelme_json_pth)
labelme_data = json.load(f)

null_keys = ['shapes', 'imagePath', 'imageData', 'imageHeight', 'imageWidth']
base_data = labelme_data.copy()
for k in null_keys:
    base_data[k] = None

# Store all shape data
shapes_data = labelme_data[null_keys[0]]
rectangle_points = [d['points'] for d in shapes_data] 

# Load Image into arr
print("Loading Image")
img_pil = PIL.Image.open(img_pth).convert("RGB")
img_arr = img_data_to_arr(img_pil_to_data(img_pil))
h, w, _ = img_arr.shape
totaln = 0

def sliding_window(imgarr, stepSize, windowSize):
    print(f"\nSliding Window with\nWindowSize: {windowSize}\nStep Size: {stepSize}")
    for y in range(0, imgarr.shape[0], stepSize[0]):
        for x in range(0, imgarr.shape[1], stepSize[1]):
            new_img_arr = imgarr[y:y+windowSize[0], x:x+windowSize[1]]
            h,w,_ = new_img_arr.shape
            if (h,w) != windowSize:
                #print(new_img_arr.shape)
                break
            else:
                yield(x,y, new_img_arr)
    print(f"Done sliding window")

def satisfy_boundary(rect, p):
    if isRectangleOverlap(rect,p):
        if isRectInRect(rect,p):
            return True
        else:
            return False
    else:
        return True
    
def test(shapes_data,labelmeRectangles, base_data, rect ,window_arr, folder_pth:str, img_name:str,write:bool) -> bool:
    # If there are no OVERLAP, 
    # and 
    if all(satisfy_boundary(rect, p) for p in labelmeRectangles):
        # Copy Base data 
        new_data = base_data.copy()

        # Get Image and load to imageData
        #cropped_image_arr = img_arr[y1:y2, x1:x2]
        new_data["shapes"] = []
        new_data["imagePath"] = img_name+".png"
        new_data["imageData"] = img_arr_to_b64(window_arr)
        new_data["imageHeight"], new_data["imageWidth"], _= window_arr.shape

        # Getting Shapes Variable
        img = img_arr_to_PIL(window_arr)
        img1 = ImageDraw.Draw(img)
        inside_rects_index = inside(rect, labelmeRectangles)
        for r2_index in inside_rects_index:
            cur_shape_data = shapes_data[r2_index].copy()
            r2 = cur_shape_data["points"]
            x1,y1 = rect[0][0], rect[0][1]
            r2x1,r2x2,r2y1,r2y2 = r2[0][0]-x1, r2[1][0]-x1, r2[0][1]-y1, r2[1][1]-y1
            cur_shape_data["points"] = [[r2x1,r2y1],[r2x2,r2y2]]
            new_data["shapes"].append(cur_shape_data)
            
            if not write:
                img1.rectangle([(r2x1,r2y1),(r2x2,r2y2)], fill="red", outline="yellow")
        if not write:
            if ubuntu:
                os.system('pkill eog')
                img.show()
            else:
                plt.imshow(img_pil_to_arr(img))
                plt.show(block=True)
                plt.close()
        else:
            print(f"Writing to img name: [{img_name}]")
            img.save(folder_pth+img_name+".png")
            with open(f'{folder_pth}{img_name}.json', 'w') as f:
                json.dump(new_data, f, indent=2)
        img.close()
        return True
    else:
        return False
        

                
for i in range(ratio[0], ratio[1], -5):
    new_h, new_w = int(i*h/100), int(i*w/100)
    new_shape = (new_h,new_w)
    stepsize = (int((h-new_h)/n_strides),int((w - new_w)/n_strides))
    windows = sliding_window(img_arr, stepsize, new_shape)
    
    n = 0
    for j, window in enumerate(windows):
        x,y,window_arr = window
        rect = window_to_rectangle(x,y,new_w,new_h)
        if test(
            shapes_data=shapes_data,
            labelmeRectangles = rectangle_points,
            base_data = base_data,
            rect = rect,
            window_arr = window_arr,
            folder_pth = folder_pth,
            img_name = f"{original_file_name}_{i}_{j}",
            write = write
        ):
            n+=1
        del x , y, window_arr
    print(f'\nFor [{i}%] \n(h, w)   = [{new_shape}]\nstepSize = [{stepsize}]\nNumImgs  = [{n}]\nn_strides = [{n_strides}]')
    totaln+=n
    n_strides = int(n_strides*multipler)

print(totaln)