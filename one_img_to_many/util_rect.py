
def isRectangleOverlap(r1:list, r2:list):
    r1x1, r1x2, r1y1, r1y2= r1[0][0], r1[1][0], r1[0][1], r1[1][1]
    r2x1, r2x2, r2y1, r2y2= r2[0][0], r2[1][0], r2[0][1], r2[1][1]
    def in_range(val, val_min, val_max):
        return (val>val_min and val<val_max)
    xOverlap = in_range(r1x1, r2x1, r1x2) or in_range(r2x1, r1x1, r1x2)
    yOverlap = in_range(r1y1, r2y1, r2y2) or in_range(r2y1, r1y1, r1y2)
    #print('Overlap', xOverlap and yOverlap)
    return xOverlap and yOverlap

def isRectInRect(r1:list, r2:list):
    r1x1, r1x2, r1y1, r1y2= r1[0][0], r1[1][0], r1[0][1], r1[1][1]
    r2x1, r2x2, r2y1, r2y2= r2[0][0], r2[1][0], r2[0][1], r2[1][1]
    r2Inr1 = (r1x1 < r2x1 < r2x2 < r1x2) and (r1y1 < r2y1 < r2y2 < r1y2)
    r1Inr2 = (r2x1 < r1x1 < r1x2 < r2x2) and (r2y1 < r1y1 < r1y2 < r2y2)
    #print('rectinrect',r2Inr1 or r1Inr2)
    return r2Inr1 or r1Inr2

# Checks if a list of rectangles are within rectangle 1:rect1
def inside(rect1:list, pts_data:list):
    rtn_pt_lst = []
    for i, pts in enumerate(pts_data):
        if isRectInRect(rect1, pts): rtn_pt_lst.append(i)
    return rtn_pt_lst

def window_to_rectangle(x,y,w,h):
    return ((x,y),(x+w,y+h))